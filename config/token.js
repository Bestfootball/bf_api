const jwt = require('jsonwebtoken');
const env = process.env.NODE_ENV || 'development';
const config = require('./config.json')[env]

// Generate an Access Token for the given User ID
module.exports=  {

  generateAccessToken(user, userId) {
    let secret, expiresIn, token

    if(!userId) {
      expiresIn = 60 * 60 * 24 * 7
      secret = config.authentication.jwtSecret

      token = jwt.sign(user, secret, {
        expiresIn: expiresIn
      })

    } else {
      const audience = config.authentication.token.audience.default
      const issuer = config.authentication.token.issuer.default

      expiresIn = 60 * 60 * 24 * 7     
      secret = process.env.JWT_SIGNING_KEY

      token = jwt.sign({}, secret, {
        expiresIn: expiresIn,
        audience: audience,
        issuer: issuer,
        subject: userId.toString()
      });
    }

    return token;
  }
}

