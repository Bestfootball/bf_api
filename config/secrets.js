//Vapid keys have been generated then saved in our .env file. 
// They are used for the notification process

const webPush = require('web-push');
/*const vapidKeys = webPush.generateVAPIDKeys()
console.log(vapidKeys.publicKey) 
console.log(vapidKeys.privateKey)*/

// Set the webpush config to send notification from server to front
webPush.setGCMAPIKey(process.env.FCM_API_KEY)
webPush.setVapidDetails(
  'mailto: administration@bestfootball.fr',
  process.env.VAPID_PUBLIC_KEY,
  process.env.VAPID_PRIVATE_KEY
); 
module.exports = {
	webPush: webPush,
  fcm: 'AAAAgKPZkq4:APA91bFgEw9qovyM8aGHPfwDPUMd3no_2Z-1vGNz0OljNhZ0YxII15T1zOsq_phirdjflnzOPrKW5cN_QPWE-OujlVy_T-BhVnmYKRVmza4jBJAqarenqbsCbmBISyUkO6-1s0zaHPav',
};